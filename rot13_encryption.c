/*
<rot13>

Kai DeLorenzo
kjdelorenzo@middlebury.edu
10/03/2018
Collaborators: None

To compile: gcc rot13_encryption.c -Wall -lm -o rot13
To run: ./rot13 <string>...
This function prints out all input strings encrypted using rot13.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


/*
This function encrypts all strings in the array by switching out capital 
and lowercase letters with their counterpart 13 letters down the alphabet.
*/
void rot13(int numlines, char *text[])
{
    for (int i = 0; i < numlines; i++)
    {
        for (int j = 0; j < strlen(text[i]); j++)
        {
            if (text[i][j] >= 65 && text[i][j] <= 90)
            {
                text[i][j] = (text[i][j] - 65 + 13) % 26 + 65;
            }
            if (text[i][j] >= 97 && text[i][j] <= 122)
            {
                text[i][j] = (text[i][j] - 97 + 13) % 26 + 97;
            }
        }
    }
}

int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        printf("Usage: %s <strings>...\n", argv[0]);
        exit(0);
    }
    rot13(argc - 1, &argv[1]);
    for (int i = 1; i < argc; i++)
    {
        printf("%s ", argv[i]);
    }
    printf("\n");
}