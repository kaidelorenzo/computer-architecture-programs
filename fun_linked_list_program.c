/*

Name:   Kai DeLorenzo
Email:  kjdelorenzo@middlebury.edu
Date:   11/14/2018
Collaborators: None

To compile: gcc fun_linked_list_program.c -o linked_list -Wall
To run:     ./linked_list
*/
#include <stdio.h>
#include <stdlib.h>

#define USAGE "Commands are a single character followed optionally by a number. The valid commands are:\n\t- a <num> (add a number to the list)\n\t- p (print the list)\n\t- r <num> (remove value <num> from the list if present)\n\t- q (quit)"

struct node
{
	int value;
	struct node *next;
};

typedef struct
{
	struct node *head;
	int length;
	struct node *tail;
} list;

list *create_list()
{
	list *l = (list *)malloc(sizeof(list));
	l->length = 0;
	return l;
}

list *add_value(list *l, int num)
{
	struct node *node = (struct node *)malloc(sizeof(struct node));
	node->value = num;
	l->length++;
	if (l->length == 1)
	{
		l->head = node;
		l->tail = node;
		node->next = node;
	}
	else
	{
		l->tail->next = node;
		l->tail = node;
		node->next = l->head;
	}
	return l;
}

list *remove_value(list *l, int num)
{
	if (l->length != 0)
	{
		struct node *pointer = l->head;
		if (pointer->value == num)
		{
			l->head = pointer->next;
			l->tail->next = l->head;
			free(pointer);
			l->length--;
			return l;
		}
		while (pointer->next != l->tail)
		{
			if (pointer->next->value == num)
			{
				struct node *temp = pointer->next;
				pointer->next = pointer->next->next;
				free(temp);
				l->length--;
				return l;
			}
			pointer = pointer->next;
		}
		if (pointer->next->value == num)
		{
			free(pointer->next);
			l->tail = pointer;
			l->tail->next = l->head;
			l->length--;
			return l;
		}
	}

	printf("%d is not in the list\n", num);
	return l;
}

void print(list *l)
{
	printf("[%d] ", l->length);
	struct node *pointer = l->head;
	if (l->length != 0)
	{
		do
		{
			printf("%d ", pointer->value);
			pointer = pointer->next;
		} while (pointer != l->head);
	}
	printf("\n");
}

int main(int argc, char *argv[])
{
	int done = 0;
	char input[32];
	char cmd;
	int data;
	int count;
	list *l = create_list();

	printf("%s\n", USAGE);

	while (!done)
	{
		printf("Enter a command: ");
		if (fgets(input, 32, stdin) == NULL)
		{
			printf("%s\n", USAGE);
		}
		else
		{
			count = sscanf(input, "%c %d\n", &cmd, &data);

			switch (cmd)
			{
			case 'a':
				if (count != 2)
				{
					printf("a should be followed by a number to add\n");
				}
				else
				{
					l = add_value(l, data);
				}
				break;
			case 'r':
				if (count != 2)
				{
					printf("r should be followed by a number to remove\n");
				}
				else
				{
					l = remove_value(l, data);
				}
				break;
			case 'p':
				print(l);
				break;
			case 'q':
				done = 1;
				break;
			default:
				printf("%s\n", USAGE);
			}
		}
	}
	return 0;
}
