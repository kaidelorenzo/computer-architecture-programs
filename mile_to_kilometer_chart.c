#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
/*

Name: Kai DeLorenzo
Email: kjdelorenzo@middlebury.edu
Date: 09/19/2018
Collaborators: None

To compile: gcc mile_to_kilometer_chart.c -lm -o gen_chart
To run:     Usage: ./gen_chart start end step=1

I hardened my program in various ways:
    - check the number of arguments
    - check for valid characters (digits 0-9)
    - check the sign
    - allow step to be optional
*/
void printTable(int start, int end, int step)
{
    // print the conversion table
    int column = 5;

    if (end > 99999)
    {
        column = floor(log10(end)) + 1;
    }

    char dashes[column + 3];
    dashes[column + 2] = '\0';
    char dashes2[column + 8];
    dashes[column + 7] = '\0';

    for (int i = 0; i < column + 2; i++)
    {
        //printf("%d", i);
        dashes[i] = '-';
    }
    for (int i = 0; i < column + 7; i++)
    {
        dashes2[i] = '-';
    }

    printf("+%s+%s+\n| %*s | %*s |\n+%s+%s+\n", dashes, dashes2, column, "miles", column + 5, "kilometers", dashes, dashes2);

    for (int i = start; i < end; i += step)
    {
        printf("| %*d |%*.2f |\n", column, i, column + 6, i * 1.609344);
    }
    printf("+%s+%s+\n", dashes, dashes2);
}

int getNumber(char *str)
{
    // extract valid number from string
    char *endptr;
    long val;

    errno = 0;
    val = strtol(str, &endptr, 10);

    if ((errno == ERANGE) || (errno != 0 && val == 0))
    {
        printf("Input too large");
        return -1;
    }

    if (endptr == str)
    {
        printf("No digits were found\n");
        return -1;
    }

    if (*endptr != '\0')
    {
        printf("Enter a number in digit form\n");
        return -1;
    }
    return val;
}

int main(int argc, char *argv[])
{
    // generate conversion table

    if (argc != 4 && argc != 3)
    {
        printf("Usage: ./hw1 start end step=1\n");
        exit(0);
    }

    int start = getNumber(argv[1]);
    int end = getNumber(argv[2]);
    int step;
    if (argc == 3)
    {
        step = 1;
    }
    else
    {
        step = getNumber(argv[3]);
    }
    if (start <= -1 || end <= -1 || step <= -1 || step == 0)
    {
        printf("Enter valid positive integers\n");
        printf("Usage: ./hw1 start end step=1\n");
        exit(0);
    }
    printTable(start, end, step);
}