/*
<base2base>

Kai DeLorenzo
kjdelorenzo@middlebury.edu
09/26/2018
Collaborators: None

To compile: gcc base_converter.c -Wall -lm -o base_converter
To run: usage: number "current base" "target base"
This program converts a positive or negative number in "current base" 
to a number in a "target base"
It is valid up through base 36 using capital letters as digits after 9
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define BASE 10
#define ASCII_1 48
#define ASCII_2 57
#define ASCII_3 65
#define ASCII_4 90
#define ASCII_5 55

int myStrtol(char string[], int base)
/*
Convert a string in base base to it's integer value
*/
{
    int result = 0;
    int length = strlen(string);
    int negative = 0;

    for (int i = 0; string[i] != 0; i++)
    {
        int valid = 0;
        if (i == 0)
        {
            if (string[i] == 45)
            {
                negative = 1;
                valid = 1;
            }
        }
        if (string[i] >= ASCII_1 && string[i] <= ASCII_2)
        {
            valid = 1;
            int number = (string[i] - ASCII_1);
            if (number >= base)
            {
                valid = 0;
            }
            result += number * pow(base, length - i - 1);
        }
        if (string[i] >= ASCII_3 && string[i] <= ASCII_4)
        {
            valid = 1;
            int number = (string[i] - ASCII_5);
            if (number >= base)
            {
                valid = 0;
            }
            result += number * pow(base, length - i - 1);
        }
        if (!valid)
        {
            printf("Please enter valid numbers\n");
            printf("usage: number \"current base\" \"target base\"\n");
            exit(0);
        }
    }
    if (negative)
    {
        return -result;
    }
    else
    {
        return result;
    }
}

void changeBase(int number, int base)
/* 
Prints the representation in base base of the integer number
*/
{
    if (number == 0)
    {
        printf("0\n");
        exit(0);
    }

    int length = floor(log(abs(number)) / log(base)) + 1;
    int negative = 0;
    if (number < 0)
    {
        negative = 1;
        length++;
    }
    number = abs(number);
    char stringNum[length + 1];
    stringNum[length] = 0;

    if (negative)
    {
        stringNum[0] = '-';
    }

    int count = 1;
    while (number != 0)
    {
        int digit = number % base;
        if (digit >= 0 && digit <= 9)
        {
            stringNum[length - count] = digit + ASCII_1;
        }
        if (digit >= BASE && digit <= 36)
        {
            stringNum[length - count] = digit + ASCII_5;
        }
        number = floor(number / base);
        count++;
    }

    printf("%s\n", stringNum);
}

int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        printf("usage: number \"current base\" \"target base\"\n");
        exit(0);
    }

    int currentBase = myStrtol(argv[2], BASE);
    int targetBase = myStrtol(argv[3], BASE);
    if (currentBase < 2 || targetBase < 2 || currentBase > 36 || targetBase > 36)
    {
        printf("Please enter a valid base\n");
        printf("usage: number \"current base\" \"target base\"\n");
        exit(0);
    }

    int input = myStrtol(argv[1], currentBase);
    changeBase(input, targetBase);
}