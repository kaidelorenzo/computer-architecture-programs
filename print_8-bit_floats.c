/*
<print 8-bit floats>

Kai DeLorenzo
kjdelorenzo@middlebury.edu
10/10/2018
Collaborators: None

To compile: gcc print_8-bit_floats.c -Wall -lm -o print_floats
To run: ./print_floats

This program prints the decimal values of all possible numbers 
represented as 8-bit floating point
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define BASE 10
#define ASCII_1 48
#define ASCII_2 87

int readSign(int num)
/*
reads sign from 8-bit floating point representation
*/
{
    return (num & 128) >> 7;
}
int readExponent(int num)
/*
reads exponent from 8-bit floating point representation
*/
{
    return ((num & 112) >> 4) - 3;
}
int readMantissa(int num)
/*
reads mantissa from 8-bit floating point representation
*/
{
    return num & 15;
}

void changeBase(int number, int base) //taken from previous assignment
/* 
Prints the representation in base base of the integer number
*/
{
    if (number < 16)
    {
        printf("0");
    }
    if (number == 0)
    {
        printf("0: ");
    }
    else
    {
        int length = floor(log(abs(number)) / log(base)) + 1;
        int negative = 0;
        if (number < 0)
        {
            negative = 1;
            length++;
        }
        number = abs(number);
        char stringNum[length + 1];
        stringNum[length] = 0;

        if (negative)
        {
            stringNum[0] = '-';
        }

        int count = 1;
        while (number != 0)
        {
            int digit = number % base;
            if (digit >= 0 && digit <= 9)
            {
                stringNum[length - count] = digit + ASCII_1;
            }
            if (digit >= BASE && digit <= 36)
            {
                stringNum[length - count] = digit + ASCII_2;
            }
            number = floor(number / base);
            count++;
        }

        printf("%s: ", stringNum);
    }
}

int main(int argc, char *argv[])
{
    for (int i = 0; i < 256; i++)
    {
        changeBase(i, 16);
        int exponent = readExponent(i);
        int num;
        double ans;
        if (readSign(i))
        {
            printf("-");
        }

        if (exponent == -3)
        {
            exponent++;
            num = readMantissa(i);
        }
        else
        {
            num = readMantissa(i) + 16;
        }
        if (exponent == 4)
        {
            if (num == 16)
            {
                ans = INFINITY;
            }
            else
            {
                ans = NAN;
            }
        }
        else
        {
            int left = num >> (4 - exponent);
            int maskHelp = pow(2, (4 - exponent)) - 1;
            int right = num & maskHelp;
            ans = left + right / pow(2, 4 - exponent);
        }
        printf("%f\n", ans);
    }
}