/*
  This program looks for instances of a substring within another string. 
  If the substring is found, the index is printed out. If it is not, 
  The user is told that the string is not present.

  to compile: gcc -o find find_substring.c -Wall

  to run: ./find <substring> <string to search>

  Examples:
  (Note that we can use quotes to make longer strings that include spaces)
  
  ./find d abcdefg
  "d" found at index 3
  ./find abc ababdecabcd
  "abc" found at index 7
  ./find dalek "The daleks are invading!"
  "dalek" found at index 4
  ./find x abcd
  "x" was not found
*/

#include <stdio.h>
#include <stdlib.h>

/*
  This function returns the index of the first occurrence of the string
  substring in the string src. It returns -1 if the substring cannot
  be found. 
*/
int find_substring(char *substring, char *src)
{
  int location = 0;

  while (src[location] != 0)
  {
    int count = location;
    int sub_count = 0;
    while (src[count] == substring[sub_count])
    {
      sub_count++;
      count++;
      if (substring[sub_count] == 0)
      {
        return location;
      }
    }
    location++;
  }
  return -1;
}

int main(int argc, char *argv[])
{

  if (argc != 3)
  {
    printf("Usage: %s <substring> <string to search>\n", argv[0]);
    exit(-1);
  }

  int index = find_substring(argv[1], argv[2]);

  if (index == -1)
  {

    printf("\"%s\" was not found\n", argv[1]);
  }
  else
  {
    printf("\"%s\" found at index %d\n", argv[1], index);
  }
}
