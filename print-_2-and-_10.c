/*
<binary>

Kai DeLorenzo
kjdelorenzo@middlebury.edu
10/03/2018
Collaborators: None

To compile: gcc print-_2-and-_10.c -Wall -lm -o binary
To run: ./binary <natural number>

This program prints the decimal and binary representations from zero up
through the given number
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("Usage: %s number\n", argv[0]);
        exit(0);
    }
    int number = atoi(argv[1]);
    if (number <= 0)
    {
        printf("Please enter a valid natural number\n");
        printf("Usage: %s <number>\n", argv[0]);
        exit(0);
    }

    int length10 = floor(log10(number)) + 1;
    int length = floor(log2(number)) + 1;
    char stringNum[length + 1];
    stringNum[length] = 0;
    for (int i = 0; i < length; i++)
    {
        stringNum[i] = '0';
    }

    for (int i = 0; i <= number; i++)
    {
        int current = i;
        int count = 1;
        while (current != 0)
        {
            int digit = current % 2;
            stringNum[length - count] = digit + 48;
            current = floor(current / 2);
            count++;
        }
        printf("%*d  %s\n", length10, i, stringNum);
    }
}