/*
Name:   Kai DeLorenzo
Email:  kjdelorenzo@middlebury.edu
Date:   11/14/2018
Collaborators: None

To compile: gcc fun_array_list_program.c -o array -Wall
To run:     ./array
*/

#include <stdio.h>
#include <stdlib.h>

#define USAGE "Commands are a single character followed optionally by a number. The valid commands are:\n\t- a <num> (add a number to the list)\n\t- p (print the list)\n\t- r <num> (remove value <num> from the list if present)\n\t- q (quit)"

typedef struct
{
    int size;   // size of list
    int length; // length of array
    int *array;
} list;

list *create_list()
{
    list *l = (list *)malloc(sizeof(list));
    l->length = 5;
    l->size = 0;
    l->array = (int *)calloc(l->length, sizeof(int));

    return l;
}

list *add_value(list *l, int num)
{
    if (l->length == l->size)
    {
        l->length = l->length * 2;
        int *array = (int *)calloc(l->length, sizeof(int));
        for (int i = 0; i < l->size; i++)
        {
            array[i] = l->array[i];
        }
        free(l->array);
        l->array = array;
    }
    l->array[l->size] = num;
    l->size++;
    return l;
}

list *remove_value(list *l, int num)
{
    for (int i = 0; i < l->size; i++)
    {
        if (l->array[i] == num)
        {
            for (int j = i; j < l->size - 1; j++)
            {
                l->array[j] = l->array[j + 1];
            }
            l->size--;
            return l;
        }
    }
    printf("%d is not in the list\n", num);
    return l;
}

void print(list *l)
{
    printf("[%d] ", l->size);
    for (int i = 0; i < l->size; i++)
    {
        printf("%d ", l->array[i]);
    }
    printf("\n");
}

int main(int argc, char *argv[])
{
    int done = 0;
    char input[32];
    char cmd;
    int data;
    int count;
    list *l = create_list();

    printf("%s\n", USAGE);

    while (!done)
    {
        printf("Enter a command: ");
        if (fgets(input, 32, stdin) == NULL)
        {
            printf("%s\n", USAGE);
        }
        else
        {
            count = sscanf(input, "%c %d\n", &cmd, &data);

            switch (cmd)
            {
            case 'a':
                if (count != 2)
                {
                    printf("a should be followed by a number to add\n");
                }
                else
                {
                    l = add_value(l, data);
                }
                break;
            case 'r':
                if (count != 2)
                {
                    printf("r should be followed by a number to remove\n");
                }
                else
                {
                    l = remove_value(l, data);
                }
                break;
            case 'p':
                print(l);
                break;
            case 'q':
                done = 1;
                break;
            default:
                printf("%s\n", USAGE);
            }
        }
    }
    return 0;
}
