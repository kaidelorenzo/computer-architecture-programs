This is a self modifying assembly program that runs for the largest finite number of steps possible on the
 Zniac II architecture without using any conditional branch instructions. 
The program also succesfully executes the halt command at the end of the program.

### Run

```shell
gcc -Wall -o zniacII zniacII.c
./zniacII -q runlong.hex
```

Note that any other C or C++ compiler should also work

### Runlong Hall of Fame

https://web.archive.org/web/20180825000646/http://www.cs.middlebury.edu:80/~schar/courses/cs202-s18/hw/hw3-results.html